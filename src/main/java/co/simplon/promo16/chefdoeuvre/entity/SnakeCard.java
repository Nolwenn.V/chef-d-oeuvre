package co.simplon.promo16.chefdoeuvre.entity;

import java.time.LocalDate;

public class SnakeCard {
    private Integer card_id;
    private Integer snake_id;
    private LocalDate date;
    private Boolean meal;
    private Boolean moulting;
    private String medicalChecking;
    private Double length;
    private Double weight;
    private String preyType;
    private Snake snake;

    public SnakeCard(Integer snake_id, LocalDate date, Boolean meal, Boolean moulting, String medicalChecking,
            Double length, Double weight, String preyType, Snake snake) {
        this.snake_id = snake_id;
        this.date = date;
        this.meal = meal;
        this.moulting = moulting;
        this.medicalChecking = medicalChecking;
        this.length = length;
        this.weight = weight;
        this.preyType = preyType;
        this.snake = snake;
    }

    public SnakeCard(LocalDate date, Boolean meal, Boolean moulting, String medicalChecking, Double length,
            Double weight, String preyType) {
        this.date = date;
        this.meal = meal;
        this.moulting = moulting;
        this.medicalChecking = medicalChecking;
        this.length = length;
        this.weight = weight;
        this.preyType = preyType;
    }

    public SnakeCard() {
    }

    public SnakeCard(Integer card_id, Integer snake_id, LocalDate date, Boolean meal, Boolean moulting,
            String medicalChecking, Double length, Double weight, String preyType) {
        this.card_id = card_id;
        this.snake_id = snake_id;
        this.date = date;
        this.meal = meal;
        this.moulting = moulting;
        this.medicalChecking = medicalChecking;
        this.length = length;
        this.weight = weight;
        this.preyType = preyType;
    }

    public Integer getCard_id() {
        return card_id;
    }

    public void setCard_id(Integer card_id) {
        this.card_id = card_id;
    }

    public Integer getSnake_id() {
        return snake_id;
    }

    public void setSnake_id(Integer snake_id) {
        this.snake_id = snake_id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Boolean getMeal() {
        return meal;
    }

    public void setMeal(Boolean meal) {
        this.meal = meal;
    }

    public Boolean getMoulting() {
        return moulting;
    }

    public void setMoulting(Boolean moulting) {
        this.moulting = moulting;
    }

    public String getMedicalChecking() {
        return medicalChecking;
    }

    public void setMedicalChecking(String medicalChecking) {
        this.medicalChecking = medicalChecking;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getPreyType() {
        return preyType;
    }

    public void setPreyType(String preyType) {
        this.preyType = preyType;
    }

    public Snake getSnake() {
        return snake;
    }

    public void setSnake(Snake snake) {
        this.snake = snake;
    }

    @Override
    public String toString() {
        return "SnakeCard [card_id=" + card_id + ", date=" + date + ", length=" + length + ", meal=" + meal
                + ", medicalChecking=" + medicalChecking + ", moulting=" + moulting + ", preyType=" + preyType
                + ", snake=" + snake + ", snake_id=" + snake_id + ", weight=" + weight + "]";
    }

}
