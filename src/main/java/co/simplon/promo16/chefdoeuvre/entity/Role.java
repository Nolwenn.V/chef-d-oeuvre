package co.simplon.promo16.chefdoeuvre.entity;

public class Role {
    private Integer role_id;
    private String description;

    public Role(String description) {
        this.description = description;
    }

    public Role(Integer role_id, String description) {
        this.role_id = role_id;
        this.description = description;
    }

    public Role() {
    }

    public Integer getRole_id() {
        return role_id;
    }

    public void setRole_id(Integer role_id) {
        this.role_id = role_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Role [description=" + description + ", role_id=" + role_id + "]";
    }

}
