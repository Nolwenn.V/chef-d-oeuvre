package co.simplon.promo16.chefdoeuvre.entity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

public class Snake {
    private Integer snake_id;
    private String name;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthDate;
    private List<SnakeCard> snakeCardList= new ArrayList<>();

    public Snake(String name, LocalDate birthDate) {
        this.name = name;
        this.birthDate = birthDate;
    }

    public Snake(Integer snake_id, String name, LocalDate birthDate) {
        this.snake_id = snake_id;
        this.name = name;
        this.birthDate = birthDate;
    }

    public Snake() {
    }

    public Integer getSnake_id() {
        return snake_id;
    }

    public void setSnake_id(Integer snake_id) {
        this.snake_id = snake_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return "Snake [birthDate=" + birthDate + ", name=" + name + ", snake_id=" + snake_id + "]";
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public List<SnakeCard> getSnakeCardList() {
        return snakeCardList;
    }

    public void setSnakeCardList(List<SnakeCard> snakeCardList) {
        this.snakeCardList = snakeCardList;
    }

}
