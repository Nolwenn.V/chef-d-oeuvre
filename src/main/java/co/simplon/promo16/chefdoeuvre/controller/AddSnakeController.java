package co.simplon.promo16.chefdoeuvre.controller;


import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import co.simplon.promo16.chefdoeuvre.entity.Snake;
import co.simplon.promo16.chefdoeuvre.entity.SnakeCard;
import co.simplon.promo16.chefdoeuvre.repository.SnakeCardRepository;
import co.simplon.promo16.chefdoeuvre.repository.SnakeRepository;

@Controller
public class AddSnakeController {

    @Autowired
    SnakeRepository repoSnake;

    @Autowired
    SnakeCardRepository repoCard;


    @GetMapping("/add-snake")
    public String showForm(Model model) {
        model.addAttribute("snake", new Snake());
        return "add-snake";
    }


    @PostMapping("/add-snake")
    public String processForm(Snake snake) {
        repoSnake.addSnake(snake);
        SnakeCard snakeCard = new SnakeCard(snake.getSnake_id(), LocalDate.now(), false, false, " ", 0d, 0d, " ", snake);
        repoCard.addSnakeCard(snakeCard, snake.getSnake_id());
        return "redirect:/account";
    }


}
