package co.simplon.promo16.chefdoeuvre.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.promo16.chefdoeuvre.entity.SnakeCard;
import co.simplon.promo16.chefdoeuvre.repository.SnakeCardRepository;

@RestController
public class ApiSnakeCardController {

    @Autowired
    SnakeCardRepository cardRepo;

    @PostMapping("/snake-card/{id}/post")
    public ResponseEntity<String> processFormCard(@PathVariable int id, @RequestBody SnakeCard card) {
        if (cardRepo.addSnakeCard(card, id)) {
            return ResponseEntity.ok().build();

        } else {
            return ResponseEntity.internalServerError().build();
        }

    }
}
