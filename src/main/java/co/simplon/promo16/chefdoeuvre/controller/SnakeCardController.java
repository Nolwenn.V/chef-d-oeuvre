package co.simplon.promo16.chefdoeuvre.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import co.simplon.promo16.chefdoeuvre.entity.Snake;
import co.simplon.promo16.chefdoeuvre.entity.SnakeCard;
import co.simplon.promo16.chefdoeuvre.repository.SnakeCardRepository;
import co.simplon.promo16.chefdoeuvre.repository.SnakeRepository;

@Controller
public class SnakeCardController {

    @Autowired
    SnakeRepository repoSnake;

    @Autowired
    SnakeCardRepository repoCard;

    @GetMapping("/snake-card/{id}")
    public String showSnakeCard(@PathVariable int id, Model model) {
        Snake snake = repoSnake.findSnakeById(id);
        List<SnakeCard> snakeCardList = repoCard.findAllCardBySnakeId(id);
        snake.setSnakeCardList(snakeCardList);
        model.addAttribute("snake", snake);
        return "snake-card";
    }

}
