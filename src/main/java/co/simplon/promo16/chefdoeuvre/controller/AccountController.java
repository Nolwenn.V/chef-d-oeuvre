package co.simplon.promo16.chefdoeuvre.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import co.simplon.promo16.chefdoeuvre.entity.User;
import co.simplon.promo16.chefdoeuvre.repository.SnakeRepository;

@Controller
public class AccountController {
    @Autowired
    SnakeRepository repo;

    @GetMapping("/account")
    public String showAccount(Authentication authentication,Model model) {
        User user = (User) authentication.getPrincipal();
        System.out.println(user.getEmail());
        model.addAttribute("snakes",repo.findAllSnakes());
        return "account";
    }


}