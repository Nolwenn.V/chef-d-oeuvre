package co.simplon.promo16.chefdoeuvre;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class ChefDOeuvreApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChefDOeuvreApplication.class, args);
	}

}
