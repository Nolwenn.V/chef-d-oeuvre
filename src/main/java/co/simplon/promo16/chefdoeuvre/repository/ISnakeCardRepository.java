package co.simplon.promo16.chefdoeuvre.repository;

import java.util.List;
import co.simplon.promo16.chefdoeuvre.entity.SnakeCard;

public interface ISnakeCardRepository {
    public boolean addSnakeCard(SnakeCard snakeCard, int snake_id);

    public SnakeCard findSnakeCardbyId(Integer id);

    public List<SnakeCard> findAllSnakeCards();

    public void updateSnakeCard(SnakeCard snakeCard);

    public void deleteSnakeCardById(Integer id);

    public List<SnakeCard> findAllByMeal(Integer id);

    public List<SnakeCard> findAllByMoulting(Integer id);

    public List<SnakeCard> findAllByLength(Integer id);

    public List<SnakeCard> findAllByWeight(Integer id);

    public SnakeCard findSnakeCardBySnakeId(Integer snakeId);

    public List<SnakeCard> findAllCardBySnakeId(Integer id);

}
