package co.simplon.promo16.chefdoeuvre.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.chefdoeuvre.entity.User;

@Repository
public class UserRepository implements IUserRepository, UserDetailsService {

    private Connection cnx;
    @Autowired
    private DataSource dataSource;

    @Override
    public void addUser(User user, int role) {
        try {
            cnx = dataSource.getConnection();
            if (user.getUser_id() != null) {
                updateUser(user);
            }
            PreparedStatement stmt = cnx
                    .prepareStatement("INSERT INTO users (userName,email,password,role_id) VALUES (?,?,?,?)",
                            PreparedStatement.RETURN_GENERATED_KEYS);
            stmt.setString(1, user.getUserName());
            stmt.setString(2, user.getEmail());
            stmt.setString(3, user.getPassword());
            stmt.setInt(4, role);

            if (stmt.executeUpdate() == 1) {
                ResultSet result = stmt.getGeneratedKeys();
                result.next();
                user.setUser_id(result.getInt(1));

            }
            cnx.close();
        } catch (SQLException e) {

            e.printStackTrace();
        }

    }

    @Override
    public User findUserById(Integer id) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement("SELECT * FROM users WHERE user_id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new User(
                        result.getInt("user_id"),
                        result.getInt("role_id"),
                        result.getString("userName"),
                        result.getString("email"),
                        result.getString("password"));
            }
            cnx.close();
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;

    }

    @Override
    public List<User> findAllUsers() {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement("SELECT * FROM users");
            ResultSet result = stmt.executeQuery();
            List<User> userList = new ArrayList<>();
            while (result.next()) {
                User user = new User(
                        result.getInt("user_id"),
                        result.getInt("role_id"),
                        result.getString("userName"),
                        result.getString("email"),
                        result.getString("password"));
                userList.add(user);
            }
            cnx.close();
            return userList;

        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void updateUser(User user) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
                    .prepareStatement("UPDATE users SET role_id=?, userName=?, email=?, password=? WHERE user_id=?");
            stmt.setInt(1, user.getRole_id());
            stmt.setString(2, user.getUserName());
            stmt.setString(3, user.getEmail());
            stmt.setString(4, user.getPassword());

            stmt.setInt(5, user.getUser_id());

            stmt.executeUpdate();
            cnx.close();
        } catch (SQLException e) {

            e.printStackTrace();
        }
    }

    @Override
    public void deleteUserById(Integer id) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
                    .prepareStatement("DELETE FROM users WHERE user_id=?");
            stmt.setInt(1, id);
            stmt.executeUpdate();
            cnx.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public User findByEmail(String email) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
                    .prepareStatement("SELECT * FROM users WHERE email=?");
            stmt.setString(1, email);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new User(
                        result.getInt("user_id"),
                        result.getInt("role_id"),
                        result.getString("userName"),
                        result.getString("email"),
                        result.getString("password"));
            }
            cnx.close();
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = findByEmail(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }
        return user;
    }

}
