package co.simplon.promo16.chefdoeuvre.repository;

import java.util.List;

import co.simplon.promo16.chefdoeuvre.entity.Snake;

public interface ISnakeRepository {
    public void addSnake(Snake snake);

    public Snake findSnakeById(Integer id);

    public List<Snake> findAllSnakes();

    public void updateSnake(Snake snake);

    public void deleteSnakeById(Integer id);

}
