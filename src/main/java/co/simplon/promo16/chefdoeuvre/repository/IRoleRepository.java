package co.simplon.promo16.chefdoeuvre.repository;

import java.util.List;

import co.simplon.promo16.chefdoeuvre.entity.Role;

public interface IRoleRepository {
    public void addRole(Role role);

    public Role findRoleById(Integer id);

    public List<Role> findAllRoles();

    public void updateRole(Role role);

    public void deleteRoleById(Integer id);

}
