package co.simplon.promo16.chefdoeuvre.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.chefdoeuvre.entity.SnakeCard;

@Repository
public class SnakeCardRepository implements ISnakeCardRepository {

    private Connection cnx;
    @Autowired
    private DataSource dataSource;

    @Override
    public boolean addSnakeCard(SnakeCard snakeCard, int snake_id) {
        try {
            cnx = dataSource.getConnection();
            if (snakeCard.getCard_id() != null) {
                updateSnakeCard(snakeCard);
            }
            PreparedStatement stmt = cnx
                    .prepareStatement(
                            "INSERT INTO snake_cards (snake_id, date, meal, moulting, medicalChecking, length, weight, preyType) VALUES (?,?,?,?,?,?,?,?)",
                            PreparedStatement.RETURN_GENERATED_KEYS);

            stmt.setInt(1, snake_id);
            stmt.setDate(2, Date.valueOf(snakeCard.getDate()));
            stmt.setBoolean(3, snakeCard.getMeal());
            stmt.setBoolean(4, snakeCard.getMoulting());
            stmt.setString(5, snakeCard.getMedicalChecking());
            stmt.setDouble(6, snakeCard.getLength());
            stmt.setDouble(7, snakeCard.getWeight());
            stmt.setString(8, snakeCard.getPreyType());

            if (stmt.executeUpdate() == 1) {
                ResultSet result = stmt.getGeneratedKeys();
                result.next();
                snakeCard.setCard_id(result.getInt(1));
                cnx.close();
                return true;
            }

        } catch (SQLException e) {

            e.printStackTrace();

        }
        return false;
    }

    @Override
    public SnakeCard findSnakeCardbyId(Integer id) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement("SELECT * FROM snake_cards WHERE card_id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new SnakeCard(
                        result.getInt("card_id"),
                        result.getInt("snake_id"),
                        result.getDate("date").toLocalDate(),
                        result.getBoolean("meal"),
                        result.getBoolean("moulting"),
                        result.getString("medicalChecking"),
                        result.getDouble("length"),
                        result.getDouble("weight"),
                        result.getString("preyType"));
            }
            cnx.close();
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<SnakeCard> findAllSnakeCards() {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement("SELECT * FROM snake_cards");
            ResultSet result = stmt.executeQuery();
            List<SnakeCard> snakeCardList = new ArrayList<>();
            while (result.next()) {
                SnakeCard snakeCard = new SnakeCard(result.getInt("card_id"),
                        result.getInt("snake_id"),
                        result.getDate("date").toLocalDate(),
                        result.getBoolean("meal"),
                        result.getBoolean("moulting"),
                        result.getString("medicalChecking"),
                        result.getDouble("length"),
                        result.getDouble("weight"),
                        result.getString("preyType"));

                snakeCardList.add(snakeCard);
            }
            cnx.close();
            return snakeCardList;

        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void updateSnakeCard(SnakeCard snakeCard) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
                    .prepareStatement(
                            "UPDATE snake_cards SET snake_id=?, date=?, meal=?, moulting=?, medicalChecking=?, length=?, weight=?, preyType=? WHERE card_id=?");
            stmt.setInt(1, snakeCard.getSnake_id());
            stmt.setDate(2, Date.valueOf(snakeCard.getDate()));
            stmt.setBoolean(3, snakeCard.getMeal());
            stmt.setBoolean(4, snakeCard.getMoulting());
            stmt.setString(5, snakeCard.getMedicalChecking());
            stmt.setDouble(6, snakeCard.getLength());
            stmt.setDouble(7, snakeCard.getWeight());
            stmt.setString(8, snakeCard.getPreyType());
            stmt.setInt(9, snakeCard.getCard_id());

            stmt.executeUpdate();
            cnx.close();
        } catch (SQLException e) {

            e.printStackTrace();
        }
    }

    @Override
    public void deleteSnakeCardById(Integer id) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
                    .prepareStatement("DELETE FROM snake_cards WHERE card_id=?");
            stmt.setInt(1, id);
            stmt.executeUpdate();
            cnx.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public List<SnakeCard> findAllByMeal(Integer id) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement("SELECT * FROM snake_cards WHERE snake_id=? AND meal=1");
            ResultSet result = stmt.executeQuery();
            List<SnakeCard> snakeCardList = new ArrayList<>();
            while (result.next()) {
                SnakeCard snakeCard = new SnakeCard(result.getInt("card_id"),
                        result.getInt("snake_id"),
                        result.getDate("date").toLocalDate(),
                        result.getBoolean("meal"),
                        result.getBoolean("moulting"),
                        result.getString("medicalChecking"),
                        result.getDouble("length"),
                        result.getDouble("weight"),
                        result.getString("preyType"));

                snakeCardList.add(snakeCard);
            }
            cnx.close();
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<SnakeCard> findAllByMoulting(Integer id) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement("SELECT * FROM snake_cards WHERE snake_id=? AND moulting=1");
            ResultSet result = stmt.executeQuery();
            List<SnakeCard> snakeCardList = new ArrayList<>();
            while (result.next()) {
                SnakeCard snakeCard = new SnakeCard(result.getInt("card_id"),
                        result.getInt("snake_id"),
                        result.getDate("date").toLocalDate(),
                        result.getBoolean("meal"),
                        result.getBoolean("moulting"),
                        result.getString("medicalChecking"),
                        result.getDouble("length"),
                        result.getDouble("weight"),
                        result.getString("preyType"));

                snakeCardList.add(snakeCard);
            }
            cnx.close();
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<SnakeCard> findAllByLength(Integer id) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement("SELECT length FROM snake_cards WHERE snake_id=?");
            ResultSet result = stmt.executeQuery();
            List<SnakeCard> snakeCardList = new ArrayList<>();
            while (result.next()) {
                SnakeCard snakeCard = new SnakeCard(result.getInt("card_id"),
                        result.getInt("snake_id"),
                        result.getDate("date").toLocalDate(),
                        result.getBoolean("meal"),
                        result.getBoolean("moulting"),
                        result.getString("medicalChecking"),
                        result.getDouble("length"),
                        result.getDouble("weight"),
                        result.getString("preyType"));

                snakeCardList.add(snakeCard);
            }
            cnx.close();
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<SnakeCard> findAllByWeight(Integer id) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement("SELECT weight FROM snake_cards WHERE snake_id=?");
            ResultSet result = stmt.executeQuery();
            List<SnakeCard> snakeCardList = new ArrayList<>();
            while (result.next()) {
                SnakeCard snakeCard = new SnakeCard(result.getInt("card_id"),
                        result.getInt("snake_id"),
                        result.getDate("date").toLocalDate(),
                        result.getBoolean("meal"),
                        result.getBoolean("moulting"),
                        result.getString("medicalChecking"),
                        result.getDouble("length"),
                        result.getDouble("weight"),
                        result.getString("preyType"));

                snakeCardList.add(snakeCard);
            }
            cnx.close();
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;
    }

    @Override
    public SnakeCard findSnakeCardBySnakeId(Integer snakeId) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement("SELECT * FROM snake_cards WHERE snake_id=?");
            stmt.setInt(1, snakeId);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new SnakeCard(
                        result.getInt("card_id"),
                        result.getInt("snake_id"),
                        result.getDate("date").toLocalDate(),
                        result.getBoolean("meal"),
                        result.getBoolean("moulting"),
                        result.getString("medicalChecking"),
                        result.getDouble("length"),
                        result.getDouble("weight"),
                        result.getString("preyType"));
            }
            cnx.close();
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<SnakeCard> findAllCardBySnakeId(Integer id) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement("SELECT * FROM snake_cards WHERE snake_id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            List<SnakeCard> snakeCardList = new ArrayList<>();
            while (result.next()) {
                SnakeCard snakeCard = new SnakeCard(result.getInt("card_id"),
                        result.getInt("snake_id"),
                        result.getDate("date").toLocalDate(),
                        result.getBoolean("meal"),
                        result.getBoolean("moulting"),
                        result.getString("medicalChecking"),
                        result.getDouble("length"),
                        result.getDouble("weight"),
                        result.getString("preyType"));

                snakeCardList.add(snakeCard);
            }
            cnx.close();
            return snakeCardList;

        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;
    }

}
