package co.simplon.promo16.chefdoeuvre.repository;

import java.util.List;


import co.simplon.promo16.chefdoeuvre.entity.User;

public interface IUserRepository {
    public void addUser(User user, int role);

    public User findUserById(Integer id);

    public List<User> findAllUsers();

    public void updateUser(User user);

    public void deleteUserById(Integer id);

    public User findByEmail(String email);

}
