package co.simplon.promo16.chefdoeuvre.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.chefdoeuvre.entity.Snake;

@Repository
public class SnakeRepository implements ISnakeRepository {

    private Connection cnx;
    @Autowired
    private DataSource dataSource;

    @Override
    public void addSnake(Snake snake) {
        try {
            cnx = dataSource.getConnection();
            if (snake.getSnake_id() != null) {
                updateSnake(snake);
            }
            PreparedStatement stmt = cnx
                    .prepareStatement("INSERT INTO snakes (name,birthDate) VALUES (?,?)",
                            PreparedStatement.RETURN_GENERATED_KEYS);

            stmt.setString(1, snake.getName());
            stmt.setDate(2, Date.valueOf(snake.getBirthDate()));

            if (stmt.executeUpdate() == 1) {
                ResultSet result = stmt.getGeneratedKeys();
                result.next();
                snake.setSnake_id(result.getInt(1));

            }
            cnx.close();
        } catch (SQLException e) {

            e.printStackTrace();
        }

    }

    @Override
    public Snake findSnakeById(Integer id) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement("SELECT * FROM snakes WHERE snake_id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new Snake(
                        result.getInt("snake_id"),
                        result.getString("name"),
                        result.getDate("birthDate").toLocalDate());
            }
            cnx.close();
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Snake> findAllSnakes() {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement("SELECT * FROM snakes");
            ResultSet result = stmt.executeQuery();
            List<Snake> snakeList = new ArrayList<>();
            while (result.next()) {
                Snake snake = new Snake(
                        result.getInt("snake_id"),
                        result.getString("name"),
                        result.getDate("birthDate").toLocalDate());

                snakeList.add(snake);
            }
            cnx.close();
            return snakeList;

        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void updateSnake(Snake snake) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
                    .prepareStatement("UPDATE snakes SET snake_id=?, name=?, birthDate=? WHERE snake_id=?");
            stmt.setInt(1, snake.getSnake_id());
            stmt.setString(1, snake.getName());
            stmt.setDate(2, Date.valueOf(snake.getBirthDate()));

            stmt.setInt(5, snake.getSnake_id());

            stmt.executeUpdate();
            cnx.close();
        } catch (SQLException e) {

            e.printStackTrace();
        }
    }

    @Override
    public void deleteSnakeById(Integer id) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
                    .prepareStatement("DELETE FROM snakes WHERE snake_id=?");
            stmt.setInt(1, id);
            stmt.executeUpdate();
            cnx.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
