package co.simplon.promo16.chefdoeuvre.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.chefdoeuvre.entity.Role;

@Repository
public class RoleRepository implements IRoleRepository {

    private Connection cnx;
    @Autowired
    private DataSource dataSource;

    @Override
    public void addRole(Role role) {
        try {
            cnx = dataSource.getConnection();
            if (role.getRole_id() != null) {
                updateRole(role);
            }
            PreparedStatement stmt = cnx
                    .prepareStatement("INSERT INTO roles (description) VALUES (?)",
                            PreparedStatement.RETURN_GENERATED_KEYS);

            stmt.setString(1, role.getDescription());

            if (stmt.executeUpdate() == 1) {
                ResultSet result = stmt.getGeneratedKeys();
                result.next();
                role.setRole_id(result.getInt(1));

            }
            cnx.close();
        } catch (SQLException e) {

            e.printStackTrace();
        }

    }

    @Override
    public Role findRoleById(Integer id) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement("SELECT * FROM roles WHERE role_id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new Role(
                        result.getInt("id"),
                        result.getString("description"));
            }
            cnx.close();
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;

    }

    @Override
    public List<Role> findAllRoles() {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement("SELECT * FROM roles");
            ResultSet result = stmt.executeQuery();
            List<Role> roleList = new ArrayList<>();
            while (result.next()) {
                Role role = new Role(
                        result.getInt("id"),
                        result.getString("description"));
                roleList.add(role);
            }
            cnx.close();
            return roleList;

        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;

    }

    @Override
    public void updateRole(Role role) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
                    .prepareStatement("UPDATE roles SET description=? WHERE role_id=?");
            stmt.setString(1, role.getDescription());

            stmt.setInt(2, role.getRole_id());

            stmt.executeUpdate();
            cnx.close();
        } catch (SQLException e) {

            e.printStackTrace();
        }

    }

    @Override
    public void deleteRoleById(Integer id) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx
                    .prepareStatement("DELETE FROM roles WHERE role_id=?");
            stmt.setInt(1, id);
            stmt.executeUpdate();
            cnx.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}
