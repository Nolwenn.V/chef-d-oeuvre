DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS roles;
DROP TABLE IF EXISTS snake_cards;
DROP TABLE IF EXISTS snakes;



CREATE TABLE roles (  
    role_id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    description VARCHAR(255) NOT NULL
) DEFAULT CHARSET UTF8;


CREATE TABLE users (  
    user_id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    role_id INTEGER,
    userName VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    FOREIGN KEY (role_id) REFERENCES roles(role_id)
) DEFAULT CHARSET UTF8;


CREATE TABLE snakes (  
    snake_id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    birthDate DATE
) DEFAULT CHARSET UTF8;

CREATE TABLE snake_cards (  
    card_id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    snake_id INTEGER,
    date DATE,
    meal TINYINT,
    moulting TINYINT,
    medicalChecking VARCHAR(255),
    length INTEGER,
    weight INTEGER,
    preyType VARCHAR(255),
    FOREIGN KEY (snake_id) REFERENCES snakes(snake_id)
) DEFAULT CHARSET UTF8;

INSERT INTO roles(description) VALUES ('ROLE_USER');

INSERT INTO snakes(name,birthDate) VALUES('Onigiri','2018-01-01');
INSERT INTO snakes(name,birthDate) VALUES('Chat','2018-01-28');
